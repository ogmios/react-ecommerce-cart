import React from "react";
import { Container, Row } from "react-bootstrap";
import Product from "../Product";
import Loading from "../Loading";
import useMediaQuery from "../../hooks/useMediaQuery";

export default function Products(props) {
  const {
    products: { result, loading, error },
    addProductCart,
  } = props;
  const isMobile = useMediaQuery("max-width", 700);
  return (
    <Container>
      <Row style={{ display: !isMobile ? "flex" : "block" }}>
        {loading || !result ? (
          <Loading />
        ) : (
          result.map((product, index) => (
            <Product
              key={index}
              product={product}
              addProductCart={addProductCart}
            />
          ))
        )}
      </Row>
    </Container>
  );
}
